ARG commit

FROM node:14-alpine AS base
ARG commit
WORKDIR /app

COPY package*.json ./
COPY ./src src
COPY ./public public
RUN npm install
RUN apk add sed

FROM base AS builder 
WORKDIR /app
ARG commit
RUN sed -i "s/Emoji Search/${commit}/" ./src/Header.js
RUN npm run build

FROM node:14-alpine AS prod
WORKDIR /app
RUN chown -R node:node /app
RUN npm install -g serve
USER node
COPY --from=builder /app/build build

EXPOSE 5000

CMD ["serve", "build"]
