Emoji Search
---

Created with *create-react-app*. See the [full create-react-app guide](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).



Install
---

`npm install`



Usage
---

`npm start`

Develop
---

In order to run this application in a local Kubernetes environment you will will need to install at least [Docker](https://docs.docker.com/), [Minikube](https://minikube.sigs.k8s.io/docs/start/), and [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/). If you would like to use [Tilt](https://docs.tilt.dev/install.html) you will need to install that as well. Once these are installed you can start the application.

Start minikube and connect it to your local docker instance:

```
minikube start
minikube -p minikube docker-env
eval $(minikube -p minikube docker-env)
```

If you are using Tilt, you can simply run `tilt up` at this point to build and initialize the application. The application will be updated on any changes and will be visible in the browser after refresh. You can then load the application in your browser with the following:

```
minikube service emoji-search
```

If you would like to run the application without Tilt, run the following:

```
docker build -t emoji-search . --build-arg commit=$(git rev-parse HEAD)
kubectl apply -f manifest.yml
minikube service emoji-search
```

To run the application using Docker alone:

```
docker build -t emoji-search . --build-arg commit=$(git rev-parse HEAD)
docker run -p 5000:5000 emoji-search
```

Security Concerns
---

Addressed:

- Application running as non-root user in the container
- Switched from development server to production-ready static server

Unaddressed:

- Deployments not configured to use HTTPS
- Application dependecies have vulnerabilities detailed by running `yarn audit`:

497 vulnerabilities found - Packages audited: 1666
Severity: 485 Low | 12 High

Potential Improvements
---

- This application does not need to be utilizing StatefulSets and could be simplified by using a Deployment without persistent volumes.
- It would be helpful to run tests either as part of the Tilt process or inside the container on each start. This would probably need to be done in a separate Dockerfile specifically for local development to avoid redundancy during deployment.
